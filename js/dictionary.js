/**
 * Created by slavkoglisic on 10/29/17.
 */
var dict = {
    "Home": {
        es: "Inicio"
    },
    "About Us": {
    es: "¿Quiénes Somos?"
    },
    "Services": {
        es: "Servicios"
    },
    "Technologies": {
        es: "Tecnologías"
    },
    "Career": {
        es: "Oportunidades laborales"
    },
    "Contact": {
    es: "Contacto"
    },
    "Our Services": {
        es: "Nuestros Servicios"
    },
    "We build value for your business by using the Smart Way": {
        es: "Desarrollamos valor para su negocio usando opciones inteligentes"
    },
    "Simple. High quality. Efficient.": {
        es: "Simple. Alta calidad. Eficiente"
    },
    "Realising your ideas, by finding or creating the best solution.": {
        es: "Realizamos sus ideas, buscando o creando una mejor solución."
    },
    "Innovative software engineering company, specialized in highly collaborative development on a global level that provide complete software solutions.": {
        es: "Empresa de ingeniería de software innovadora, especializada en desarrollo altamente colaborativo a nivel global que brinda soluciones completas."
    },
    "Founded and located in Santiago, Chile (UTC-3), specialized in Front and Backend Design/Development, Machine Learning and QA.": {
        es: "Fundada y ubicada en Santiago de Chile (UTC-3), especializada en diseño / desarrollo de front-end y backend, Machine Learning y QA."
    },
    "Experts in software engineering that constantly follow the latest world class standards.": {
        es: "Expertos en ingeniería de software que constantemente siguen los últimos estándares de clase mundial."
    },
    "Recognizing the need is the primary condition for success.": {
        es: "Reconocer la necesidad es la condición principal para el éxito."
    },
    "Architecture": {
        es:"Arquitectura"
    },
    "Development and Design": {
        es:"Diseño y Desarollo",
        en:"Development & Design"
    },
    "Project Management": {
        es:"Gestión de Proyecto"
    },
    "Solutions based on cloud computing, microservices, and best IT industry practices.": {
        es:"Soluciones basadas en cloud computing, microservicios y las mejores prácticas de la industria de TI."
    },
    "Designing a solution and creating a look and feel of the product. Deciding on its structure and selecting ideal approach and process. Engineering the components necessary to make that product work.": {
        es:"Diseñando  la solución y creando la apariencia del producto. Decidiendo sobre su estructura seleccionando el enfoque y el proceso ideal. Ingeniería de los componentes necesarios para que  producto funcione."
    },
    "Assuring a desired level of quality in a service or product. Focus on preventing mistakes or defects in manufactured products when delivering solutions or services to our customers. Ensure all necessary operational techniques and activities to fulfill requirements for obtaining the best quality.": {
        es:"Nuestro objetivo es asegurar un nivel deseado de calidad en el servicio o el producto. Concentrándonos en prevenir errores o defectos en los productos fabricados al brindar soluciones o servicios a nuestros clientes. Asegurando todas las técnicas y actividades operacionales necesarias para cumplir con los requisitos para obtener la mejor calidad."
    },
    "Applicate knowledge, skills, tools, and techniques in order to meet the client requirements. Plan, organize, coordinate and control all project activities to achieve defined goals. Foresee, predict and mitigate all possible risks and problems.": {
        es:"Aplicamos conocimiento, habilidades, herramientas y técnicas para cumplir con los resultados esperados. Planificando, organizando, coordinando y controlando todas las actividades del proyecto para lograr los objetivos definidos. Prevenir, predecir y mitigar todos los posibles riesgos y problemas es uno de los lemas de nuestro equipo de gestión."
    },
    "New and better ways to solve problems. Tools, skills and practices that add more value.": {
        es:"Seleccionamos las herramientas que mejor se adapten a la solución del problema, incorporando las habilidades y prácticas del equipo, así logramos excelentes resultados."
    },
    "Join Our Team": {
        es:"Únete a nuestro equipo"
    },
    "If you want to work in a collaborative environment where opportunities are offered, skills are recogniozed and excellence is rewarded, you might be exactly what we’re looking for. We seek open-minded, talented, and innovative individuals who can help us continue to offer our high standard of quality service. We offer exciting opportunities that will challenge your abilities, expand your skills and reward your contributions.": {
        es:"Si deseas trabajar en un entorno de colaboración donde se ofrecen oportunidades, se reconocen las habilidades y se premia la excelencia, puedes ser exactamente lo que estamos buscando. Buscamos individuos abiertos, talentosos e innovadores que puedan ayudarnos a continuar ofreciendo nuestro servicio de alta calidad. Ofrecemos interesantes oportunidades que pondrán a prueba tus habilidades, ampliarán tus destrezas y recompensarán tus contribuciones."
    },
    "CONTACT US": {
        es:"CONTÁCTENOS"
    },
    "Address": {
        es:"Dirección"
    },
    "Phones": {
        es:"Teléfonos"
    },
    "Language": {
        es:"Idioma"
    },
    "SUBMIT": {
        es:"ENVIAR"
    },
    "Clients": {
        es:"Clientes"
    },
    "Products": {
        es:"Productos"
    },
    "Optytek Spa is a leading consulting and engineering company located in Latin America with headquarters in Santiago de Chile. Its objective is to provide professional services of the highest quality in a comprehensive end to end manner for mobile network operators. Its specialty fields include planning, designing, optimizing and maintaining consistency for 2G, 3G and LTE mobile networks, as well as having extensive experience in converged voice and data services, IMS and VoLTE.":{
        es:"Optytek Spa es una compañía referente de consultoría e ingeniería ubicada en américa latina con sede en Santiago de Chile. Su objetivo es proporcionar servicios profesionales de la más alta calidad de forma integral end to end para operadores de redes móviles. Su especialidades incluyen la planificación, diseño, optimización y mantención de consistencia de redes móviles 2G, 3G y LTE, además de tener amplia experiencia en servicios convergentes de voz y datos, IMS y VoLTE."
    },
    "Is a Chilean department store founded in 1889 by an Italian family based in Chile. Falabella has continuously evolved to become one of the main players in the region, with a presence in Chile, Peru, Colombia, Argentina, Brazil, Uruguay and Mexico, comprising retail, marketplace, shopping centers and financial services.":{
        es:"Es una tienda por departamentos chilena fundada en 1889 por una familia italiana radicada en Chile. Falabella ha evolucionado continuamente para convertirse en uno de los principales actores de la región, con presencia en Chile, Perú, Colombia, Argentina, Brasil, Uruguay y México, a través de ofertas de retail, marketplace, centros comerciales y servicios financieros."
    },
    "OptyExpert utilizes an intelligent analytics-based, data-driven multi-site optimization engine that use CM, PM and FM data and now new trace capabilities for improved prediction results and accuracy. The simulation platform provides observability and unique way to help out a RF optimization engineer improving network statistics data and standard key performance indicators (KPIs). OptyExpert work with 3G/LTE networks regardless of infrastructure vendor (Ericsson, Nokia, Huawei).":{
        es:"OptyExpert utiliza un algoritmo avanzado de optimización de multi-sitios basado en CM, PM y FM data-driven análisis para sus simulaciones. como nueva funcionalidad ahora incorporamos la capacidad de rastreo para mejorar los resultados de predicción y la precisión. La plataforma de simulación proporciona observabilidad y una forma única de ayudar al personal de optimización RF a mejorar los datos estadísticos de la red y los indicadores clave de rendimiento (KPI). OptyExpert funciona con redes 3G / LTE independientemente del proveedor de infraestructura (Ericsson, Nokia, Huawei)."
    },
    "This application was designed to facilitate project management and internal and external teams, providing complete control and detailed management of activities where all team members can participate, and view the status of their projects and tasks. In addition, the application has the possibility of generating different reports with a complete range of filters and views. It easily adapts to the needs of each user and allows to store various configurations for future use. Another interesting aspect of the system is that all the parameters are configurable and it can be quickly coupled to a wide variety of projects and businesses.":{
        es:"Esta aplicación fue diseñada para facilitar la gestión de proyectos y los equipos internos y externos, brindando el control completo y manejo detallado de las actividades en donde todos los integrantes del equipo pueden participar, y visualizar el estado de sus proyectos y tareas. Además, la aplicación cuenta con la posibilidad de generar distintos reportes con una gama completa de filtros y vistas. Se adapta fácilmente a las necesidades de cada usuario y permite almacenar varias configuraciones para su uso en el futuro. Otro aspecto interesante del sistema, es que todos los parámetros son configurables, eso quiere decir, que se acopla rápidamente a una gran variedad de proyectos y negocios."
    },
    "The purpose of the PowerOps application is to help electric power generating companies to manage requests related to complementary services. Its current voltage and frequency control modules allow creating, monitoring, controlling and saving the requests made between the Electric System Operator, Generating Company's Operations Center and its power plants. Additionally, it has implemented the notification system, both at the system level and by email, as well as the reporting module that allows you to make and save any query over the application's data, filter and export them in different formats.": {
        es:"El propósito de la aplicación PowerOps es ayudar a las empresas generadoras de energía eléctrica gestionar las solicitudes relacionadas a los servicios complementarios. Sus módulos actuales de control de tensión y frecuencia permiten crear, hacer el seguimiento, controlar y guardar las solicitudes realizadas entre el Operador del sistema eléctrico, Centro de Operaciones de la empresa generadora y sus centrales eléctricas. Adicionalmente tiene implementado el sistema de notificaciones, tanto en el nivel del sistema, como por correo electrónico, como también el módulo de reportes que permite realizar y guardar cualquier consulta sobre los datos de la aplicación, filtrarlos y exportarlos en distintos formatos."
    },
}







